<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Rol::class)->create([
            'name'  => 'Administrador',
            ]);

        factory(App\Rol::class)->create([
            'name'  => 'Editor',
            ]);

        factory(App\Rol::class)->create([
            'name'   => 'User',
            ]);
    }
}