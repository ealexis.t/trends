<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');


//Route::get('enlace', 'EnlaceQuejaController@create');

/*
|--------------------------------------------------------------------------
| Application Routes
|-------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('home', 'HomeController@index');


/*
|--------------------------------------------------------------------------
| AdminController Routes
|-------------------------------------------------------------------
*/
    Route::get('admin/dashboard', 'AdminController@dashboard');
    Route::get('admin/users', 'AdminController@users');

/*
|--------------------------------------------------------------------------
| UserController Routes
|-------------------------------------------------------------------
*/
    Route::get('editor/home', 'EditorController@index');

/*
|--------------------------------------------------------------------------
| UserController Routes
|-------------------------------------------------------------------
*/
    Route::get('user/home', 'UserController@index');
/*
|--------------------------------------------------------------------------
| TrendController Routes
|-------------------------------------------------------------------
*/
	Route::any('addTrend', 'TrendController@add');

/*
|--------------------------------------------------------------------------
| TwitterController Routes
|-------------------------------------------------------------------
*/
	Route::get('map', 'TwitterController@map');

});