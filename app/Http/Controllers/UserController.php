<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Url;
use DB;

class UserController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');

        /*
        |----------------------------------------------------------------------------
        | Contadores de Acciones por estatus
        |--------------------------------------------------------------------
        */

    }

    public function index(){
        $url=Url::where("typeSrc","=","facebook")->get();
        
        return view('user.index', ['urls' => $url]);
    }

}