<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class EditorController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');

        /*
        |----------------------------------------------------------------------------
        | Contadores de Acciones por estatus
        |--------------------------------------------------------------------
        */

    }

    public function index(){

        return view('editor.index');
    }


}