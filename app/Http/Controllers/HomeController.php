<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        if(Auth::user()->idRol == '1'){
            return redirect()->to('admin/dashboard');
        }

        if(Auth::user()->idRol == '2'){
            return redirect()->to('editor/home');
        }

        if(Auth::user()->idRol == '3'){

            return redirect()->to('user/home');
        }

    }
}