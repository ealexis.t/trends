<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    //Use table own
    protected $table = 'urls';

    protected $fillable = [
    	'url',
        'typeSrc',
        'idUsers',
        'post',
    ];
}