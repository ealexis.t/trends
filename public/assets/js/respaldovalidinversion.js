
/*
|-----------------------------------------------------------------------------
| Validacion de Impacto Mensual
|----------------------------------------------------------------
*/
$(document).ready(function(){

    $('.impacto').numeric();
    $('.inversion').numeric();
    $('.impactoEsperado1').numeric();
    $('.impactoEsperado2').numeric();
    $('.impactoEsperado3').numeric();
    $('.impactoEsperado4').numeric();
    $('.impactoEsperado5').numeric();
    $('.impactoEsperado6').numeric();
    $('.impactoEsperado7').numeric();

   // $('.impactoEsperado').hide();
    $('.periodoFin').attr('disabled','disabled');
    $('.impactoEsperado8').hide();
    $('.impactoEsperado9').hide();
    $('.impactoEsperado10').hide();
    $('.impactoEsperado11').hide();
    $('.impactoEsperado12').hide();

    $('#invFederal').hide();
    $('#invEstatal').hide();
});

function accionPI(accion){

    var periodoInicio = $('#periodoInicio'+accion).val();

    /*
    |---------------------------------------------------
    |Activa el select periodoFin
    |-------------------------------------------
    */
    $('#periodoFin'+accion).removeAttr('disabled');
    /*
    |---------------------------------------------------
    |Borra los options del select
    |-------------------------------------------
    */
    $('.optionMes').remove();
    /*
    |---------------------------------------------------
    |valida que el select no este vacio
    |-------------------------------------------
    */

    //
    if(periodoInicio == 0){
        $('#periodoFin'+accion).attr('disabled','disabled');
    }else{
        /*
        |------------------------------------------------------
        |Se agregan la opcion de mes final del periodo
        |-----------------------------------------------
        */
        if(periodoInicio < 1){
            $('#periodoFin'+accion).append('<option class="optionMes" value="1">Enero</option>')
        }
        if(periodoInicio < 2){
            $('#periodoFin'+accion).append('<option class="optionMes" value="2">Febrero</option>')
        }
        if(periodoInicio < 3){
            $('#periodoFin'+accion).append('<option class="optionMes" value="3">Marzo</option>')
        }
        if(periodoInicio < 4){
            $('#periodoFin'+accion).append('<option class="optionMes" value="4">Abril</option>')
        }
        if(periodoInicio < 5){
            $('#periodoFin'+accion).append('<option class="optionMes" value="5">Mayo</option>')
        }
        if(periodoInicio < 6){
            $('#periodoFin'+accion).append('<option class="optionMes" value="6">Junio</option>')
        }
        if(periodoInicio < 7){
            $('#periodoFin'+accion).append('<option class="optionMes" value="7">Julio</option>')
        }
        if(periodoInicio < 8){
            $('#periodoFin'+accion).append('<option class="optionMes" value="8">Agosto</option>')
        }
        if(periodoInicio < 9){
            $('#periodoFin'+accion).append('<option class="optionMes" value="9">Septiembre</option>')
        }
        if(periodoInicio < 10){
            $('#periodoFin'+accion).append('<option class="optionMes" value="10">Octubre</option>')
        }
        if(periodoInicio < 11){
            $('#periodoFin'+accion).append('<option class="optionMes" value="11">Noviembre</option>')
        }
        if(periodoInicio < 12){
            $('#periodoFin'+accion).append('<option class="optionMes" value="13">Diciembre</option>')
        }

    }

}

function inv(e){
    var tipoInversion = $('#'+e+' #tipoInversion').val();

    var inversion = $('#'+e+' #inversion').val();

    var inversionFederal = $('#'+e+' #inversionFederal').val();
    var inversionEstatal = $('#'+e+' #inversionEstatal').val();

    if(tipoInversion === ''){
        $('#'+e+' #invFederal').hide();
        $('#'+e+' #invEstatal').hide();

        var inversionFederal = $('#'+e+' #inversionFederal').val('0');
        var inversionEstatal = $('#'+e+' #inversionEstatal').val('0');
    }

    if(tipoInversion === 'Federal'){
        $('#'+e+' #invFederal').show('slow');
        $('#'+e+' #invEstatal').hide();

        var inversionFederal = $('#'+e+' #inversionFederal').val('');
        var inversionEstatal = $('#'+e+' #inversionEstatal').val('0');
    }
    if(tipoInversion === 'Estatal'){
        $('#'+e+' #invFederal').hide();
        $('#'+e+' #invEstatal').show('slow');

        var inversionFederal = $('#'+e+' #inversionFederal').val('0');
        var inversionEstatal = $('#'+e+' #inversionEstatal').val('');
    }
    if(tipoInversion === 'Coinversion'){
        $('#'+e+' #invFederal').show('slow');
        $('#'+e+' #invEstatal').show('slow');

        var inversionFederal = $('#'+e+' #inversionFederal').val('');
        var inversionEstatal = $('#'+e+' #inversionEstatal').val('');
    }

}

var cuenta=0;

function  validarFormulario(e){


    var titulo = $('#'+e+' #titulo').val();
    var tipoMeta = $('#'+e+' #tipoMeta').val();
    var tipoInversion = $('#'+e+' #tipoInversion').val();
    var impacto = $('#'+e+' #impacto').val();
    var inversion = $('#'+e+' #inversion').val();
    var responsables = $('#'+e+' #responsables').val();
    var metodologia = $('#'+e+' #metodologia').val();

    var inversionFederal = $('#'+e+' #inversionFederal').val();
    var inversionEstatal = $('#'+e+' #inversionEstatal').val();

    var inversion = parseInt(inversionEstatal) + parseInt(inversionFederal);

    //alert(inversion);


    if(titulo.length == 0){
        $('#'+e+' #label-titulo').text('Campo Requerido');
        valid_1 = false;
    }else{
        $('#'+e+' #label-titulo').text('');
        valid_1 = true;
    }


    if(impacto.length == 0){
        $('#'+e+' #label-impacto').text('Campo Requerido');
        valid_2 = false;
    }else{
        $('#'+e+' #label-impacto').text('');
        if(impacto < 0){
            $('#'+e+' #label-impacto').text('No se permiten numeros negativos');
        }else{
            valid_2 = true;
        }
    }

    if(inversion == 0){
        $('#'+e+' #label-inversion').text('Campo Requerido');
        valid_3 = false;

    }else{
        $('#'+e+' #label-inversion').text('');
        if(inversion < 0){
            $('#'+e+' #label-inversion').text('No se permiten numeros negativos');
        }else{

            valid_3 = true;
        }
    }

    if(responsables.length == 0){
        $('#'+e+' #label-responsables').text('Campo Requerido');
        valid_4 = false;
    }else{
        $('#'+e+' #label-responsables').text('');
        valid_4 = true;
    }

    if(metodologia.length == 0){
        $('#'+e+' #label-metodologia').text('Campo Requerido');
        valid_5 = false;
    }else{
        $('#'+e+' #label-metodologia').text('');
        valid_5 = true;
    }

    if(tipoMeta.length == 0){
        $('#'+e+' #label-tipoMeta').text('Campo Requerido');
        valid_6 = false;
    }else{
        $('#'+e+' #label-tipoMeta').text('');
        valid_6 = true;
    }
    /*
    |------------------------------------------------------------------------
    |  Tipo de Inversion
    |----------------------------------------------------------------
    */
    if(tipoInversion.length == 0){
        $('#'+e+' #label-tipoInversion').text('Campo Requerido');
        valid_7 = false;
    }else{
        $('#'+e+' #label-tipoInversion').text('');
        valid_7 = true;
    }

    if(tipoInversion == 'Federal'){
        if(inversionFederal.length == 0){
            $('#'+e+' #label-inversionFederal').text('Campo Requerido');
            valid_8 = false;
        }else{
            $('#'+e+' #label-inversionFederal').text('');
            valid_8 = true;
        }
    }

    if(tipoInversion == 'Estatal'){
        if(inversionEstatal.length == 0){
            $('#'+e+' #label-inversionEstatal').text('Campo Requerido');
            valid_8 = false;
        }else{
            $('#'+e+' #label-inversionEstatal').text('');
            valid_8 = true;
        }
    }


    if(tipoInversion == 'Coinversion'){

        if(inversionFederal.length == 0){
            $('#'+e+' #label-inversionFederal').text('Campo Requerido');
            valid_8 = false;
        }else{
            $('#'+e+' #label-inversionFederal').text('');
            valid_8 = true;
        }

        if(inversionEstatal.length == 0){
            $('#'+e+' #label-inversionEstatal').text('Campo Requerido');
            valid_8 = false;
        }else{
            $('#'+e+' #label-inversionEstatal').text('');
            valid_8 = true;
        }
    }



    var impacto1 = $('#'+e+' #impactoEsperado1').val();
    var impacto2 = $('#'+e+' #impactoEsperado2').val();
    var impacto3 = $('#'+e+' #impactoEsperado3').val();
    var impacto4 = $('#'+e+' #impactoEsperado4').val();
    var impacto5 = $('#'+e+' #impactoEsperado5').val();
    var impacto6 = $('#'+e+' #impactoEsperado6').val();
    var impacto7 = $('#'+e+' #impactoEsperado7').val();
    var impacto8 = $('#'+e+' #impactoEsperado8').val();
    var impacto9 = $('#'+e+' #impactoEsperado9').val();
    var impacto10 = $('#'+e+' #impactoEsperado10').val();
    var impacto11 = $('#'+e+' #impactoEsperado11').val();
    var impacto12 = $('#'+e+' #impactoEsperado12').val();

    if(impacto1 <= 0){
        impacto1 = 0;
    }
    if(impacto2 <= 0){
        impacto2 = 0;
    }
    if(impacto3 <= 0){
        impacto3 = 0;
    }
    if(impacto4 <= 0){
        impacto4 = 0;
    }
    if(impacto5 <= 0){
        impacto5 = 0;
    }
    if(impacto6 <= 0){
        impacto6 = 0;
    }
    if(impacto7 <= 0){
        impacto7 = 0;
    }
    if(impacto8 <= 0){
        impacto8 = 0;
    }
    if(impacto9 <= 0){
        impacto9 = 0;
    }
    if(impacto10 <= 0){
        impacto10 = 0;
    }
    if(impacto11 <= 0){
        impacto11 = 0;
    }
    if(impacto12 <= 0){
        impacto12 = 0;
    }

    var impactoEsperado = parseInt(impacto1)+parseInt(impacto2)+parseInt(impacto3)+parseInt(impacto4)+parseInt(impacto5)+parseInt(impacto6)+parseInt(impacto7)+parseInt(impacto8)+parseInt(impacto9)+parseInt(impacto10)+parseInt(impacto11)+parseInt(impacto12);

    console.log(impacto1);
    console.log(impacto2);
    console.log(impacto3);
    console.log(impacto4);
    console.log(impacto5);
    console.log(impacto6);
    console.log(impacto7);
    console.log(impacto8);
    console.log(impacto9);
    console.log(impacto10);
    console.log(impacto11);
    console.log(impacto12);
    console.log('Impacto esperado = '+impacto);
    console.log('Suma impacto mensual = '+impactoEsperado);

    if(impacto != impactoEsperado || impacto <= 0){
        alert('La suma de los impactos mensuales debe ser igual al Impacto del Objetivo');
        valid_6 = false;
    }
    else{
        valid_6 = true;
    }

    var valid = valid_1*valid_2*valid_3*valid_4*valid_5*valid_6*valid_7*valid_8;

    console.log(valid);

    if(valid == 1){
        alert('Información valida.');
        $('#'+e).submit(true);
    }
    if(valid == 0){
        $('#'+e).submit(false);
    }


}
var campos = $(".impactoEsperado");
console.log(campos.length );
