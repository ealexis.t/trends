<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Trends</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">


        <script async defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCpTvQacyDTlWWxJgj92xCR5NiktiG1oc&callback=initMap">
        </script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css">
{{Html::style('/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}
{{Html::style('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}
{{Html::style('/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}
{{Html::style('/assets/global/plugins/uniform/css/uniform.default.css')}}
{{Html::style('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}
{{Html::style('/assets/global/css/components.css')}}
{{Html::style('/assets/global/css/plugins.css')}}
{{Html::style('/assets/admin/layout/css/layout.css')}}
{{Html::style('/assets/admin/layout/css/themes/light.css')}}
{{Html::style('/assets/admin/layout/css/custom.css')}}

{{Html::style('/assets/css/login.css')}}
{{Html::style('/assets/css/style.css')}}
{{Html::style('/assets/css/dropzone.css')}}
{{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>

<body class="page-header-fixed page-quick-sidebar-over-content">
@if(Session::has('message'))
    <div class="modal-success">
        <div class="message col-md-4 col-md-offset-4">
            <a class="pull-right" href="{{ URL('home') }}">Cerrar</a>
            <div class="text-center">
                <img class="icon-mensaje" src="{{ asset('/assets/image/ok.png') }}" alt="" height="100px" width="100px">
            </div>
            <hr style="border:solid #444 2px;">
            <p class="text-center">
                {{ Session::get('message') }}
            </p>
        </div>
    </div>
    <script>
        function timer(){
            $(".modal-success").hide('fast');
            setInterval(redirect, 2000);
        }
        function redirect(){
            $("modal-success").css('display', 'none');
        }
        setInterval(timer, 1000);
    </script>
@endif
@if(Session::has('message-error'))
    <div class="modal-success">
        <div class="message col-md-4 col-md-offset-4">
            <a class="pull-right" href="{{ URL('home') }}">Cerrar</a>
            <div class="text-center">
                <img class="icon-mensaje" src="{{ asset('/assets/image/error.png') }}" alt="" height="100px" width="100px">
            </div>
            <hr style="border:solid #444 2px;">
            <p class="text-center">
                {{ Session::get('message-error') }}
            </p>
        </div>
    </div>
    <script>
        function timer(){
            $(".modal-success").hide('fast');
            setInterval(redirect, 3000);
        }
        function redirect(){
            $("modal-success").css('display', 'none');
        }
        setInterval(timer, 2000);
    </script>
@endif
<!-- BEGIN HEADER -->

<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/home" style="font-size:20px; color:#ddd;">
            Trends
            </a>
            <div class="menu-toggler sidebar-toggler hide">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <a href="#" style="color:#fff;" class="dropdown-toggle">{{ Auth::user()->nombre }}</a>
                </li>
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <a href="{{URL::to('logout')}}" style="color:#fff;" class="dropdown-toggle">
                    Cerrar Sesión
                    </a>
                </li>
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->

    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->

            <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="sidebar-toggler-wrapper">
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <div class="sidebar-toggler">
                    </div>
                    <!-- END SIDEBAR TOGGLER BUTTON -->
                </li>
                <li class="sidebar-search-wrapper">
                    <br>
                </li>
                @if(Auth::user()->idRol == '3')
                <li class="start @if(Request::is('home')) active @endif">
                    <a href="{{URL::to('home')}}">
                    <i class="icon-home"></i>
                    <span class="title">Panel General</span>
                    </a>
                </li>
                <li class="start @if(Request::is('map')) active @endif">
                    <a href="{{URL::to('map')}}">
                    <i class="icon-map"></i>
                    <span class="title">Mapa de tendencias</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->idRol != '3')
                <li class="start @if(Request::is('home')) active @endif">
                    <a href="{{URL::to('home')}}">
                    <i class="icon-home"></i>
                    <span class="title">Inicio</span>
                    </a>
                </li>
                <li class="start @if(Request::is('enlace/acciones/*')) active @endif">
                    <a href="javascript:;">
                    <i class="icon-equalizer" style="font-size:10px;"></i><span class="title">Tendencias por categoría</span>
                    <span class="arrow @if(Request::is('enlace/acciones/*')) open @endif"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="@if(Request::is('categoria/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            México
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('categoria/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            Mundo
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('categoria/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            Politica
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('categoria/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            Espectáculos
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('categoria/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            Deportes
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('categoria/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            Ciencia y Salud
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('categoria/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            Cultura
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('categoria/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            Tecnología
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="start @if(Request::is('red/*')) active @endif">
                    <a href="javascript:;">
                    <i class="fa fa-sitemap"></i>
                    <span class="title">Red de personajes</span>
                    <span class="arrow @if(Request::is('enlace/acciones/*')) open @endif"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="@if(Request::is('red/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            México
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('red/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            Opinión Pública
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('red/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            Líderes empresariales
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="start @if(Request::is('situacion/*')) active @endif">
                    <a href="javascript:;">
                    <i class="icon-graph"></i>
                    <span class="title">SEDESOL Situación</span>
                    <span class="arrow @if(Request::is('enlace/acciones/*')) open @endif"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="@if(Request::is('situacion/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            SEDESOL
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('situacion/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            PROSPERA
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('situacion/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            LICONSA
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('red/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            DICONSA
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('situacion/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            INAPAM
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('situacion/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            INDESOL
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('situacion/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            CONADIS
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('situacion/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            FONART
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                        <li class="@if(Request::is('situacion/*')) open @endif">
                            <a href="{{URL::to('/home')}}">
                            <i class="icon-speech"></i>
                            INAES
                            <span class="badge badge-danger"></span>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
                @if(Auth::user()->rol == '2')
                <li class="start @if(Request::is('home')) active @endif">
                    <a href="{{URL::to('home')}}">
                    <i class="icon-home"></i>
                    <span class="title">Inicio</span>
                    </a>
                </li>
                @endif

                @if(Auth::user()->rol == '1')
                <li class="start @if(Request::is('home')) active @endif">
                    <a href="{{URL::to('home')}}">
                    <i class="icon-home"></i>
                    <span class="title">Inicio</span>
                    </a>
                </li>
                <li class="start @if(Request::is('admin/users')) active @endif">
                    <a href="{{URL::to('admin/users')}}">
                    <i class="icon-users"></i>
                    <span class="title">Users</span>
                    </a>
                </li>
                @endif

            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    </div>

    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">

        <div class="page-content" style="min-height:1323px">
            @yield('content')
        </div>
    </div>

</div>

<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>

{{Html::script('/assets/global/plugins/jquery.min.js')}}
{{Html::script('/assets/global/plugins/jquery-migrate.min.js')}}
{{Html::script('/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js')}}
{{Html::script('/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}
{{Html::script('/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}
{{Html::script('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}
{{Html::script('/assets/global/plugins/jquery.blockui.min.js')}}
{{Html::script('/assets/global/plugins/jquery.cokie.min.js')}}
{{Html::script('/assets/global/plugins/uniform/jquery.uniform.min.js')}}
{{Html::script('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}
{{Html::script('/assets/global/scripts/metronic.js')}}
{{Html::script('/assets/admin/layout/scripts/layout.js')}}
{{Html::script('/assets/admin/layout/scripts/quick-sidebar.js')}}
{{Html::script('/assets/admin/layout/scripts/demo.js')}}
{{Html::script('/assets/js/bootstrap-filestyle.js')}}
{{Html::script('/assets/js/dropzone.js')}}
{{Html::script('/assets/js/jquery.validate.min.js')}}
<script>

    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
    });
</script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
@yield('js-extras')
</body>
</html>