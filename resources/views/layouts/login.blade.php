<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Trends</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <!--link href="assets/css/bootstrap.min.css" rel="stylesheet"-->
    {{Html::style('assets/css/bootstrap.min.css')}}
    {{Html::style('assets/css/login.css')}}
    {{Html::style('assets/css/style.css')}}
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
            background-color: #edede9;
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    @if(Session::has('message'))
        <div class="modal-success">
            <div class="message col-md-4 col-md-offset-4">
                <a class="pull-right" href="{{ URL('home') }}">Cerrar</a>
                <div class="text-center">
                    <img class="icon-mensaje" src="{{ asset('/assets/image/ok.png') }}" alt="" height="100px" width="100px">
                </div>
                <hr style="border:solid #444 2px;">
                <p class="text-center">
                    {{ Session::get('message') }}
                </p>
            </div>
        </div>
        <script>
            function timer(){
                $(".modal-success").hide('fast');
                setInterval(redirect, 1000);
            }
            function redirect(){
                $("modal-success").css('display', 'none');
            }
            setInterval(timer, 1000);
        </script>
    @endif
    @yield('modals')
    <nav class="navbar navbar-default menu">
        <div class="container">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/home') }}" style="color:#fff;">
                    Trends
                </a>
            </div>
            @if (Auth::guest())
            @else

            @endif
        </div>
    </nav>
    @if(Request::is('home') && Auth::user()->rol == 'Enlace')
    <div class="banner"></div>
    @endif

    <div class="container">
        @yield('content')
    </div>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

    {{Html::script('assets/js/bootstrap.min.js')}}

    <script type="text/javascript">
    </script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    @yield('js-extras')
</body>
</html>