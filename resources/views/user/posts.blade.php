    @extends('layouts.app')
    @section('content')
    <div class="container">

        <h2><a id="step1" onclick="commets()"><strong>Publicaciones de :</strong> {{$source}}</a> <span class='step2 text-primary'> > </span> <a class='step2'>Gráficas</a></h2>
        <hr>
        <div id="comments" ></div>
        <div class="col-md-12">
            <div id="graph" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

            <div class="tiles step2">
                <div class="tile bg-blue-steel">
                    <div class="tile-body">
                        <i class="fa fa-thumbs-o-up"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Likes
                        </div>
                        <div class="number" id='likes'>
                        </div>
                    </div>
                </div>
                <button class="tile bg-red-sunglo" data-toggle="modal" data-target="#modalListUser">
                    <div class="tile-body">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Users Likes
                        </div>
                        <div class="number">

                        </div>
                    </div>
                </button>
                <div class="tile bg-green-meadow">
                    <div class="tile-body">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Comentarios Totales
                        </div>
                        <div class="number" id="totalCommets">
                        </div>
                    </div>
                </div>
    <!--
                <div class="tile double selected bg-green-turquoise">
                    <div class="corner">
                    </div>
                    <div class="check">
                    </div>
                    <div class="tile-body">
                        <h4>support@metronic.com</h4>
                        <p>
                             Re: Metronic v1.2 - Project Update!
                        </p>
                        <p>
                             24 March 2013 12.30PM confirmed for the project plan update meeting...
                        </p>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <div class="number">
                             14
                        </div>
                    </div>
                </div>
                <div class="tile selected bg-yellow-saffron">
                    <div class="corner">
                    </div>
                    <div class="tile-body">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                             Members
                        </div>
                        <div class="number">
                             452
                        </div>
                    </div>
                </div>

                <div class="tile double bg-blue-madison">
                    <div class="tile-body">
                        <img src="../../assets/admin/pages/media/profile/photo1.jpg" alt="">
                        <h4>Announcements</h4>
                        <p>
                             Easily style icon color, size, shadow, and anything that's possible with CSS.
                        </p>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                             Bob Nilson
                        </div>
                        <div class="number">
                             24 Jan 2013
                        </div>
                    </div>
                </div>
                <div class="tile double-down bg-blue-hoki">
                    <div class="tile-body">
                        <i class="fa fa-bell-o"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                             Notifications
                        </div>
                        <div class="number">
                             6
                        </div>
                    </div>
                </div>
                <div class="tile bg-purple-studio">
                    <div class="tile-body">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                             Orders
                        </div>
                        <div class="number">
                             121
                        </div>
                    </div>
                </div>
                <div class="tile double bg-grey-cascade">
                    <div class="tile-body">
                        <img src="../../assets/admin/pages/media/profile/photo2.jpg" alt="" class="pull-right">
                        <h3>@lisa_wong</h3>
                        <p>
                             I really love this theme. I look forward to check the next release!
                        </p>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            <i class="fa fa-twitter"></i>
                        </div>
                        <div class="number">
                             10:45PM, 23 Jan
                        </div>
                    </div>
                </div>

                <div class="tile bg-red-intense">
                    <div class="tile-body">
                        <i class="fa fa-coffee"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                             Meetups
                        </div>
                        <div class="number">
                             12 Jan
                        </div>
                    </div>
                </div>
                <div class="tile bg-green">
                    <div class="tile-body">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                             Reports
                        </div>
                        <div class="number">
                        </div>
                    </div>
                </div>

                <div class="tile bg-yellow-lemon selected">
                    <div class="corner">
                    </div>
                    <div class="check">
                    </div>
                    <div class="tile-body">
                        <i class="fa fa-cogs"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                             Settings
                        </div>
                    </div>
                </div>
                <div class="tile bg-red-sunglo">
                    <div class="tile-body">
                        <i class="fa fa-plane"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                             Projects
                        </div>
                        <div class="number">
                             34
                        </div>
                    </div>
                </div>
            </div>
    -->
        </div>
    </div>
    <?php
    /*
    |-----------------------------------------------------
    | Modal List Users
    |----------------------------------------------
    */
    ?>
    <div class="modal fade" id="modalListUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        {{ Form::open(array('name' => 'f1','url' => 'addTrend',  'method' => 'post','class'=>'form-horizontal row-fluid'))}}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Lista de Usuarios</h3>
            </div>
            <div class="modal-body text-center">
                <ul id="userList" class="list-unstyled"></ul>
            </div>
            <div class="modal-footer">
            </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>
    @endsection
    @section('modals')

    @endsection
    @section('js-extras')
    <script src="/assets/js/charts.js"></script>
    <script src="/assets/js/exporting.js"></script>
    <script type="text/javascript">

    $('.step2').hide();

    function commets(){
        $('#graph').hide('slow');
        $('.step2').hide();
        $('#comments').show('slow');
    }

    $('#graph').hide();

    src = '<?php echo $source ?>';

    if(1==1){
        //var postData = $("#registro").serializeArray();
        var token={
            token:"EAAO8XpK8l9gBAJpRhnPWJJrtvv3yh4UZB7oRAZB2Yivg0SZAK4MPl7e7EzDTj2iZCrZA47vM96oV7QidgKgvTxoDu1Y6r3LmPlSyH8ORSAFihNOI84Y23YJfTgxFSPpSScCY5QsJOsOGeablAKBaliHnG8ZB19yB2juiXPCMgJnAZDZD"
        };

        var source=
        {
            source: src
        };

        $.ajax({
        url: 'http://kt.sedesol.gob.mx/getPosts.php',
        method: 'GET',
        data: $.param(token) + "&" + $.param(source),
        dataType: "json",
        success: function(data) {
            var fecha=new Date();
            var horario=(fecha).getTime()
            for (var i=0; i < data["posts"].length; i++)
                {
                    if(data["posts"][i]["message"] != undefined){
                        comment = '<div class="col-md-4" id="'+data["posts"][i]["id"]+'"><div class="portlet box " style="background-color:#7CBCC4;"><div class="portlet-title"><div class="caption"><i class="fa fa-cogs"></i>POST '+ timeAgo(data["posts"][i]["created_time"]["date"],horario)+'</div><div class="tools"><a href="javascript:;" class="collapse" data-original-title="" title=""></a></div></div><div class="portlet-body" style="height:auto;"><div class="well well-lg" style="height:240px;"><h4 class="block"></h4><p>' + data["posts"][i]["message"] + '</p></div><div id="footer '+data["posts"][i]["id"]+'" class="footer"><button class="icon-btn" id="'+data["posts"][i]["id"]+'" ><i class="fa fa-comments"></i><div>Análisis de comentarios</div></button></div><br></div></div></div>';
                        $("#comments").append(comment);

                    }
                }
            }
        });
    }


    $(document).on("click",".icon-btn",function()
    {
        $('#comments').hide('slow');
        $('#graph').show('slow');
        $('.step2').show('slow');

        var idComment = $(this).attr("id");

        var token={
            token:"EAAO8XpK8l9gBAJpRhnPWJJrtvv3yh4UZB7oRAZB2Yivg0SZAK4MPl7e7EzDTj2iZCrZA47vM96oV7QidgKgvTxoDu1Y6r3LmPlSyH8ORSAFihNOI84Y23YJfTgxFSPpSScCY5QsJOsOGeablAKBaliHnG8ZB19yB2juiXPCMgJnAZDZD"
        };

        var source=
        {
            source: idComment
        }

        $.ajax({
        //headers: 'Access-Control-Allow-Origin: *',
        url: 'http://kt.sedesol.gob.mx/getComments.php',
                type:"GET",
        data:  $.param(token) + "&" + $.param(source),
        success: function(data) {

                $("span").remove(".totalCommets");
                var graph = data;
                totalCommets ='<span class="totalCommets">'+graph[10]['summary']['total_count']+'<span>';
                $("#totalCommets").append(totalCommets);

                /*
                |------------------------------------------
                | top One
                |--------------------------------------
                */
                topOneW = graph[0][0];
                topOneN = graph[0][1];
                /*
                |------------------------------------------
                | top Two
                |--------------------------------------
                */
                topTwoW = graph[1][0];
                topTwoN = graph[1][1];
                /*
                |------------------------------------------
                | top Three
                |--------------------------------------
                */
                topThreeW = graph[2][0];
                topThreeN = graph[2][1];
                /*
                |------------------------------------------
                | top Four
                |--------------------------------------
                */
                topFourW = graph[3][0];
                topFourN = graph[3][1];
                /*
                |------------------------------------------
                | top Five
                |--------------------------------------
                */
                topFiveW = graph[4][0];
                topFiveN = graph[4][1];
                /*
                |------------------------------------------
                | top Six
                |--------------------------------------
                */
                topSixW = graph[5][0];
                topSixN = graph[5][1];
                /*
                |------------------------------------------
                | top Seven
                |--------------------------------------
                */
                topSevenW = graph[6][0];
                topSevenN = graph[6][1];
                /*
                |------------------------------------------
                | top Eight
                |--------------------------------------
                */
                topEightW = graph[7][0];
                topEightN = graph[7][1];
                /*
                |------------------------------------------
                | top Nine
                |--------------------------------------
                */
                topNineW = graph[8][0];
                topNineN = graph[8][1];
                /*
                |------------------------------------------
                | top Ten
                |--------------------------------------
                */
                topTenW = graph[9][0];
                topTenN = graph[9][1];

                $(function () {
                    $('#graph').highcharts({
                        chart: {
                            type: 'bar'
                        },
                        title: {
                            text: 'Palabras clave'
                        },
                        xAxis: {
                            categories: [topOneW, topTwoW, topThreeW, topFourW, topFiveW, topSixW, topSevenW, topEightW, topNineW, topTenW]
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Top 10'
                            }
                        },
                        legend: {
                            reversed: true
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal'
                            }
                        },
                        series: [{
                            name: 'frecuencia',
                            data: [topOneN, topTwoN, topThreeN, topFourN, topFiveN, topSixN, topSevenN, topEightN, topNineN, topTenN]
                        }]
                    });
                });
            }
        });

       var tokenLikes={
            token:"EAAO8XpK8l9gBAJpRhnPWJJrtvv3yh4UZB7oRAZB2Yivg0SZAK4MPl7e7EzDTj2iZCrZA47vM96oV7QidgKgvTxoDu1Y6r3LmPlSyH8ORSAFihNOI84Y23YJfTgxFSPpSScCY5QsJOsOGeablAKBaliHnG8ZB19yB2juiXPCMgJnAZDZD"
        };

        var sourceLikes=
        {
            source: idComment
        };

        $.ajax({
        url: 'http://kt.sedesol.gob.mx/getLikes.php',
        type: 'GET',
        data: $.param(tokenLikes) + "&" + $.param(sourceLikes),
        success: function(data) {
            $("li").remove(".user");
            $("span").remove(".likes");

            var count = data.length;

            totalLike ='<span class="likes">' + data[data.length-1]['summary']['total_count']+'<span>';

            $("#likes").append(totalLike);

                for (var i=0; i < count-1; i++){
                    userItem = '<li class="user">'+data[i]['name']+'</li>';
                    $("#userList").append(userItem);
                }


            }
        });

    });
        
        
function timeAgo(time,real){
// Se le suman 18500 porque la hora de facebook que arroja está en la zona horaria de burkina faso +00:00 entonces para convertirlo a la hora de México se necesitan sumar las 5 horas de diferencia
var date = new Date((time || "").replace(/-/g,"/").replace(/\.000000/g,"")),
    diff = ((real - ( date.getTime())) / 1000)+18500,
    day_diff = Math.floor(diff / (60 * 60 * 24));

if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 31 )
    return;

return day_diff == 0 && (
        diff < 60 && "ahorita" ||
        diff < 120 && "Hace un minuto" ||
        diff < 3600 && "Hace " +Math.floor( diff / 60 ) + " minutos" ||
        diff < 7200 && "Hace una hora" ||
        diff < 86400 && "Hace "+Math.floor( diff / 3600 ) + " horas") ||
    day_diff == 1 && "Ayer" ||
    day_diff < 7 && "Hace " + day_diff + " días" ||
    day_diff < 31 && "Hace " + Math.ceil( day_diff / 7 ) + " semanas";
}
    </script>
    <style>
        #step1{
            cursor: pointer;
        }
    </style>
    @endsection