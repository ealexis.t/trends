@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-offset-9 col-md-4 controles">
            <button class="btn btn-clock btn-danger" id="carga">Actualizar tendencias <i class="fa fa-spinner"></i></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div id="map">
            </div>
        </div>
        <div class="col-md-3 trends" >
            <ul id="trends" class="list-unstyled"></ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 comments">

        </div>
    </div>
</div>
@endsection
@section('modals')

@endsection
@section('js-extras')

<style>
    .controles
    {
        margin-bottom:25px;
    }
	.list-unstyled .label{
		margin-bottom: 5px;
	}
	.trends{
		border-left: 1px solid #aaa;
		height: 450px;
		overflow: scroll;

	}
    .comments .item
    {
        padding:20px;
        border-bottom: 1px solid #eee;
        
    }
    .comments .item:hover
    {
        background-color:#fafafa;
    }
     #map { height: 450px;  }
</style>
<script>

       var map; 
       var markers = [];
       var loca=[];

   function setMapOnAll(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }

    function clearMarkers() {
      setMapOnAll(null);
      markers = [];

    }


    function initMap() {
      // Create a map object and specify the DOM element for display.
 var myLatLng = {lat:23.542825 , lng: -102.520807};
   
  // Create a map object and specify the DOM element for display.
  map = new google.maps.Map(document.getElementById('map'), {
    center: myLatLng,
    scrollwheel: false,
    //draggable: false,
    zoom: 4
  });
        
    }

            $(window).load(function()
                {
                initMap()
                });
                    $(document).on("click",".it",function(e)
                   {
                    e.preventDefault();
                var base_url = 'http://kt.sedesol.gob.mx/atc.php';
                    var func = {
                      "f": "3"
                    };
                    var source = {
                      "source": $(this).attr("r")
                    };
                    var yah;
                    var dim;
                        $(".comments").html("");
                    var data= $.param(source) +"&"+$.param(func);
                      $.ajax({
                        url: base_url ,
                        type: 'GET',
                        data: data,
                        success: function(data) {
                            console.log(data[0].length);
                            for(var i=0; i < data[0].length; i++)
                            {
                    $(".comments").append('<div class="col-md-12 item"><div class="col-md-3"><div class="col-md-12"><img src="'+data[0][i]["user"]["profile_image_url"]+'" /></div><div class="col-md-12">@'+data[0][i]["user"]["screen_name"]+'</div></div><div class="col-md-9"><p>'+data[0][i]["text"]+'</p></div><br></div>'); 
                            }
                                
                        }});
                    });
                $(document).on("click","#carga",function(e)
                   {
                    e.preventDefault();
                    var base_url = 'http://kt.sedesol.gob.mx/atc.php';
                    var func = {
                      "f": "1"
                    };
                    var yah;
                    var dim;
                    var data= $.param(func);
                      $.ajax({
                        url: base_url ,
                        method: 'GET',
                        data: data,
                        dataType:"json",
                        success: function(data) {
                            var datas=Object.keys(data).length;
                            yah=data;
                            dim=datas;
                                
                        },
                          complete:function()
                          {
                              var infowindow = new google.maps.InfoWindow;
                                    clearMarkers();
                                    var  i;
                                    for (i = 0; i < dim; i++) {  
                                       
                                        marker = new google.maps.Marker({
                                             position: new google.maps.LatLng(yah[i]["centroid"]["latitude"], yah[i]["centroid"]["longitude"]),
                                             map: map
                                        });
                                        markers.push(marker);
                                        //marker.addListener('click', toggleBounce);
                                        google.maps.event.addListener(markers[i], 'click', (function(marker, i) {
                                             return function() {
                                                 infowindow.setContent(yah[i]["name"]);
                                                 infowindow.open(map, marker);
                                                 trends(yah[i]["woeid"]);
                                             }
                                        })(marker, i));
                                    }
                          }
                });
                });

	function trends(e){
        $("#trends").html("");
	    var source =
	    {
	        source: e
	    };
	  	$.ajax({
		    url: 'http://kt.sedesol.gob.mx/atc.php?f=2',
		    method: 'GET',
		    data: "&" + $.param(source),
		    dataType: "json",
		    success: function(data) {
	    		var count = Object.keys(data).length;
	    		for (var i=0; i < count; i++){
	    			//console.log(data[i]['name']);
	    			trend = '<li class="item"><a class="it" r="'+data[i]['name']+'" href="'+data[i]['url']+'">'+data[i]['name']+'</a><li>';
	    			$("#trends").append(trend);
	    		}
	        }
	    });
	}
</script>

@endsection