@extends('layouts.app')
<style>
    .title h4
    {
        margin:0px;
        padding-top:10px;
        padding-bottom:10px;
    }
    .title img.img
    {
        position:relative;
        float:right;
        margin-top:-30px;
        margin-right:20px;
        height:20px;
    }
    .profile img
    {
        margin-top:10px;
        height:50px;
    }
    .padding-none
    {
        padding-right:0px !important;
        padding-left:0px !important;
    }
    .icon-style img
    {
        height:20px;
    }
</style>
@section('content')


<!----------------------------------------------------------------------------------------------------!-->
<div class="col-md-12">
    <div class="col-md-8">
	<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal">
		<i class="fa fa-plus"></i> Agregar página de facebook
	</button>
    </div>
    <div class="col-md-4">
    <button type="button" class="btn btn-primary btn-block" style="background-color:#1DA1F2;" data-toggle="modal" data-target="#myModal2">
		<i class="fa fa-plus"></i> Agregar página de twitter
	</button>
    </div>
</div>
<div class="container"  style="padding-top:80px !important; ">
    <div class="row facebook" >
    @foreach($urls as $url)
    <div class="col-md-3 text-center" >
        <div class="title" style="background-color:#7CBCC4; margin:0px; color:#fff;">
            <h4>{{$url->url}}</h4>
            <img class="img" src="{{ asset('/assets/image/facebook.png') }}" >  
        </div>
        <a href="{{URL::to('addTrend')}}?url={{$url->url}}">
        <div class="" style=" padding-bottom:20px; padding-top:20px; background-color:#eaeaea;">
        ANALIZAR
        </div>
        </a>
    </div>
    @endforeach
    </div>
    <hr>
    <div class="row twitter">
        <!--
        <div class="col-md-4 text-center">
            <div class="title" style="background-color:#7CBCC4; margin:0px; color:#fff;">
            <h4>@salcamarena</h4>
            <img class="img" src="{{ asset('/assets/image/twitter.png') }}" >  
                </div>
            <div class="col-md-3 profile padding-none">
                <img class="img" src="{{ asset('/assets/image/twitter.png') }}" >  
            </div>
            <div class="col-md-9 padding-none">
                <div class="" style=" padding-bottom:20px; padding-top:20px; background-color:#eaeaea;">
                La otra fuerza depredadora es la corrupción, por eso, si cae Duarte, qué le espera a Veracruz http://www.elfinanciero.com.mx/opinion/si-cae-duarte-veracruz-que.html#.V-kilgTefRI.twitter … #LaFeria
                </div>
            </div>
        </div>
        -->
    </div>
</div>


<?php
/*
|-----------------------------------------------------
| Modal Add Trend
|----------------------------------------------
*/
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    {{ Form::open(array('name' => 'f1','url' => 'addTrend',  'method' => 'post','class'=>'form-horizontal row-fluid'))}}
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Agregar nombre de página para realizar su análisis</h4>
		</div>
        <div class="modal-body text-center">
	        <div class="form-group">
	            <label class="col-md-2 control-label">Nombre:</label>
	            <div class="col-md-10">
	                <div class="input-icon">
	                    <i class="fa fa-bell-o"></i>
	                    <input name="url" type="text" class="form-control">
	                </div>
	            </div>
	        </div>

	        <button type="submit" class="btn btn-lg bg-blue">Cargar publicaciones</button>
        </div>
		<div class="modal-footer">
		</div>
    </div>
  	{{ Form::close() }}
  </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Agregar nombre perfil para realizar su análisis</h4>
		</div>
        <div class="modal-body text-center">
	        <div class="form-group">
	            <label class="col-md-2 control-label">Nombre:</label>
	            <div class="col-md-10">
	                <div class="input-icon">
	                    <i class="fa fa-bell-o"></i>
	                    <input name="url" type="text" id="url" class="form-control">
	                </div>
	            </div>
	        </div>
                <button  class="btn btn-lg bg-blue perfil" style="margin-top:20px;" class="close" data-dismiss="modal" aria-label="Close" >Cargar perfil</button>
	        
        </div>
		<div class="modal-footer">
            
		</div>
    </div>
  </div>
</div>

@endsection
@section('modals')

@endsection
@section('js-extras')
<script>
$(document).on("click",".perfil",function()
{
                    var func = {
                      "f": "4"
                    };
                    var source = {
                      "source": $("#url").val()
                    };
       $.ajax({
        url: 'http://kt.sedesol.gob.mx/atc.php',
        type: 'GET',
        data: $.param(func) + "&" + $.param(source),
        success: function(data) {
                console.log(data[0]);
            var datos=data[0];
            $(".twitter").append(
            '<div class="col-md-4 text-center">'+
            '<div class="title" style="background-color:#7CBCC4; margin:0px; color:#fff;">'+
            '<h4>'+datos["user"]["name"]+' @'+datos["user"]["screen_name"]+'</h4>'+
            '<img class="img" src="{{ asset('/assets/image/twitter.png') }}" >'+  
            '</div>'+
            '<div class="col-md-3 padding-none profile">'+
                '<img class="img" src="'+datos["user"]["profile_image_url"]+'" >'+ 
            '</div>'+
            '<div class="col-md-9 padding-none">'+
               '<div class="" style=" padding-bottom:20px; padding-top:20px; background-color:#efefef;">'+datos["text"]+'</div>'+
            '</div>'+
             '<div class="col-md-12 padding-none text-right icon-style">'+
               '<div class="" style=" padding-bottom:20px; padding-top:20px; background-color:#efefef;"><p><i class="fa fa-retweet" aria-hidden="true"></i> Retweets: '+datos["retweet_count"]+' <span style="margin-left:20px;"></span> <i class="fa fa-heart" aria-hidden="true"></i> Favs: '+datos["favorite_count"]+' <span style="margin-right:20px;"></span>  </p></div>'+
            '</div>'+
            '</div>');
            }
        });
});
</script>

@endsection