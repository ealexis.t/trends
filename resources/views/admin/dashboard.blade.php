@extends('layouts.app')
@section('content')
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="dashboard-stat green-haze">
        <div class="visual">
            <i class="icon-settings"></i>
        </div>
        <a href="{{ URL('admin/users') }}">
        <div class="details">
            <div class="number">
                Adminstración
            </div>
            <div class="desc">
                Usuarios
            </div>
        </div>
        </a>
        <a class="more" href="{{ URL('admin/users') }}">
        View more <i class="m-icon-swapright m-icon-white"></i>
        </a>
    </div>
</div>
@endsection
@section('js-extras')
@endsection